# Lilapp REST
#### Middleware to consume and simulate the modification of [FakeAPI](https://jsonplaceholder.typicode.com/)

## API Documentation URL
http://localhost:8090/lilapp-rest/av2/api-docs -> (On running project)

## Swagger URL
http://localhost:8090/lilapp-rest/swagger-ui.html -> (On running project)

## API Operations
*The following operations are deeply described in the above links (once the project is running)*

### UserController
* **(GET)** All Users paginated
* **(GET)** Single User by Id
* **(GET)** All used from FakeAPI as XML text
* **(GET)** All used from FakeAPI as XML file - *The file is lazily created on the first call and unique to avoid multilpe instances*
### PostController
* **(GET)** All Posts paginated
* **(GET)** All Posts owned by a certain User paginated
* **(GET)** Single Post by Id
* **(PATCH)** Modify the Title of a certain Post and persists the change
* **(PATCH)** Modify the Base of a certain Post and persists the change
### CommentController
* **(GET)** All Comments of a certain Post paginated
* ~~**(POST)** Modify the Base of a certain Post and persists the change~~ - *This operation was going to be added but FakeAPI returns always the same Id, making imposible to persist more than one created element. Operation is still defined in the FeignClient ready to be used.*

## Extras

### H2 in-memory DB
Due to the limitations of the [FakeAPI](https://jsonplaceholder.typicode.com/) in operability and disponibility, I found a good idea to add a database to persist data and keep the changes produced by the user. 

#### Advantages
* Control over disponibility of the service.
* Control over the data along time.
* Ensure the changes persistance.
* Some advanced query options like pagination.

#### Disadvantages
* Slower porject initialization.
* Memory requirements (in this case this is a minor problem)

### Swagger
Allows API developers to use and check API operations in a easier way through the web browser. Futhermore, includes info and documentation for integration purposes.

## Possible improvements
* Add login system (a password field has already been created in user in order to be used that way)
* Include API and Service testing
* Include Spring Security to avoid fraudulent use

