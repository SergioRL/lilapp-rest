package com.srl.lilapp.jpa.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@Entity(name = "COMMENT")
public class Comment implements Serializable {

    @Id
    @NotNull
    @Column(name = "ID", nullable = false, unique = true)
    private Long id;

    @NotNull
    @Column(name = "POST_ID", nullable = false)
    private Long postId;

    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "NAME", nullable = false)
    private String name;

    @NotNull
    @Size(min = 1, max = 2048)
    @Column(name = "BODY", nullable = false)
    private String body;

    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "EMAIL", nullable = false, unique = true)
    private String email;

}
