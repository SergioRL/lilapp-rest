package com.srl.lilapp.jpa.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@Entity(name = "POST")
public class Post implements Serializable {

    @Id
    @NotNull
    @Column(name = "ID", nullable = false, unique = true)
    private Long id;

    @NotNull
    @Column(name = "USER_ID", nullable = false)
    private Long userId;

    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "TITLE", nullable = false)
    private String title;

    @NotNull
    @Size(min = 1, max = 2048)
    @Column(name = "BODY", nullable = false)
    private String body;

}
