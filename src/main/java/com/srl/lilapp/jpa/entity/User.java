package com.srl.lilapp.jpa.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@Entity(name = "USER")
public class User implements Serializable {

    @Id
    @NotNull
    @Column(name = "ID", nullable = false, unique = true)
    private Long id;

    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "USERNAME", nullable = false, unique = true)
    private String username;

    @NotNull
    @Size(min = 1, max = 1024)
    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "EMAIL", nullable = false, unique = true)
    private String email;

    @Size(min = 1, max = 32)
    @Column(name = "PHONE")
    private String phone;

    @Size(min = 1, max = 512)
    @Column(name = "WEBSITE")
    private String website;

    @Size(min = 1, max = 128)
    @Column(name = "CITY")
    private String city;

}
