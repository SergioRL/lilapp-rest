package com.srl.lilapp.api.dto.response;

import com.srl.lilapp.jpa.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserBasicInfoDTO {

    private Long id;

    private String username;

    private String email;

    private String phone;

    /**
     * Method for getting an instance of UserBasicInfoDTO based on
     * a User instance values
     *
     * @param user  User to take the values from
     * @return      New instance of UserBasicInfoDTO with the matching values from @user
     */
    public static UserBasicInfoDTO getInstanceFromUser(User user) {

        UserBasicInfoDTO resultInstance = new UserBasicInfoDTO();

        if (user != null) {
            BeanUtils.copyProperties(user, resultInstance);
        }

        return resultInstance;
    }

}
