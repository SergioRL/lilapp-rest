package com.srl.lilapp.api.dto.response;


import com.srl.lilapp.jpa.entity.Comment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CommentDTO {

    private Long id;

    private Long postId;

    private String name;

    private String body;

    private String email;

    /**
     * Method for getting an instance of CommentDTO based on
     * a Comment instance values
     *
     * @param comment   Comment to take the values from
     * @return          New instance of CommentDTO with the matching values from @comment
     */
    public static CommentDTO getInstanceFromComment(Comment comment) {

        CommentDTO resultInstance = new CommentDTO();

        if (comment != null) {
            BeanUtils.copyProperties(comment, resultInstance);
        }

        return resultInstance;

    }
}
