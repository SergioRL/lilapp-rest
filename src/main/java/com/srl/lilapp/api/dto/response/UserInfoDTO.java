package com.srl.lilapp.api.dto.response;

import com.srl.lilapp.jpa.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserInfoDTO {

    private Long id;

    private String username;

    private String email;

    private String phone;

    private String website;

    private String city;

    /**
     * Method for getting an instance of UserInfoDTO based on
     * a User instance values
     *
     * @param user  User to take the values from
     * @return      New instance of UserInfoDTO with the matching values from @user
     */
    public static UserInfoDTO getInstanceFromUser(User user) {

        UserInfoDTO resultInstance = new UserInfoDTO();

        if (user != null) {
            BeanUtils.copyProperties(user, resultInstance);
        }

        return resultInstance;
    }

}
