package com.srl.lilapp.api.dto.response;

import com.srl.lilapp.jpa.entity.Post;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PostDTO {

    private Long id;

    private Long userId;

    private String title;

    private String body;

    /**
     * Method for getting an instance of PostDTO based on
     * a Post instance values
     *
     * @param post  Post to take the values from
     * @return      New instance of PostDTO with the matching values from @post
     */
    public static PostDTO getInstanceFromPost(Post post) {

        PostDTO resultInstance = new PostDTO();

        if (post != null) {
            BeanUtils.copyProperties(post, resultInstance);
        }

        return resultInstance;

    }

}
