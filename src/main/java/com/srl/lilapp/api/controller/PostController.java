package com.srl.lilapp.api.controller;

import com.srl.lilapp.api.dto.request.PatchPostBodyDTO;
import com.srl.lilapp.api.dto.request.PatchPostTitleDTO;
import com.srl.lilapp.api.dto.response.PostDTO;
import com.srl.lilapp.client.dto.request.ModifyPostTitleDTO;
import com.srl.lilapp.jpa.entity.Post;
import com.srl.lilapp.service.post.PostService;
import feign.FeignException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.NoSuchElementException;

@RestController
@RequestMapping(value = "/post", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.PATCH})
public class PostController {

    @Autowired
    private PostService postService;

    @GetMapping(value = "/all/paginated")
    @ApiOperation("Get all the Posts in a paginated way")
    private ResponseEntity<Page<PostDTO>> getAllPostsPaginated(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size
    ){

        if (page < 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, " @page param must by 0 or higher.");
        } else if (size < 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, " @size param must by 0 or higher.");
        }

        Page<Post> resultPage = postService.getAllPostsPaginated(page, size);

        Page<PostDTO> result =
                resultPage.map(post -> PostDTO.getInstanceFromPost(post));

        return ResponseEntity.ok(result);
    }

    @GetMapping(value = "/user-id/{userId}/paginated")
    @ApiOperation("Get all the Posts from a User in a paginated way")
    private ResponseEntity<Page<PostDTO>> getPostsByUserIdPaginated(
            @PathVariable Long userId,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size
    ){

        if (page < 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, " @page param must by 0 or higher.");
        } else if (size < 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, " @size param must by 0 or higher.");
        }

        Page<Post> resultPage = postService.getPostsByUserIdPaginated(userId, page, size);

        Page<PostDTO> result =
                resultPage.map(post -> PostDTO.getInstanceFromPost(post));

        return ResponseEntity.ok(result);
    }

    @GetMapping(value = "/{postId}")
    @ApiOperation("Get a single Post from its Id")
    private ResponseEntity<PostDTO> getPostById(@PathVariable long postId){

        PostDTO result = null;

        try {
            result = PostDTO.getInstanceFromPost(postService.getPostById(postId));
        } catch (NoSuchElementException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Post with the given @id does not exist.");
        }

        return ResponseEntity.ok(result);
    }

    @PatchMapping(value = "/{postId}/title")
    @ApiOperation("Modify the title of a specific Post.")
    private ResponseEntity<PostDTO> patchPostTitle(
            @PathVariable long postId,
            @RequestBody PatchPostTitleDTO patchPostTitleDTO){

        PostDTO result = null;

        if (patchPostTitleDTO == null ||
                !StringUtils.hasText(patchPostTitleDTO.getTitle())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Post title can't be null or empty.");
        }

        try {
            Post currentPost =
                    postService.patchPostTitle(postId, patchPostTitleDTO.getTitle());
            result = PostDTO.getInstanceFromPost(currentPost);
        } catch (FeignException ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "Some error occurred while applying changes on the FakeAPI.");
        }

        return ResponseEntity.ok(result);
    }

    @PatchMapping(value = "/{postId}/body")
    @ApiOperation("Modify the body of a specific Post.")
    private ResponseEntity<PostDTO> patchPostBody(
            @PathVariable long postId,
            @RequestBody PatchPostBodyDTO patchPostBodyDTO){

        PostDTO result = null;

        if (patchPostBodyDTO == null ||
                !StringUtils.hasText(patchPostBodyDTO.getBody())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Post body can't be null or empty.");
        }

        try {
            Post currentPost =
                    postService.patchPostBody(postId, patchPostBodyDTO.getBody());
            result = PostDTO.getInstanceFromPost(currentPost);
        } catch (FeignException ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "Some error occurred while applying changes  on the FakeAPI.");
        }

        return ResponseEntity.ok(result);
    }


}
