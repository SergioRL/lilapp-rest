package com.srl.lilapp.api.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.srl.lilapp.api.dto.response.UserBasicInfoDTO;
import com.srl.lilapp.api.dto.response.UserInfoDTO;
import com.srl.lilapp.jpa.entity.User;
import com.srl.lilapp.service.user.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.NoSuchElementException;

@RestController
@RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*", methods= {RequestMethod.GET})
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/all/paginated")
    @ApiOperation("Get all the Users in a paginated way")
    private ResponseEntity<Page<UserBasicInfoDTO>> getAllUsersPaginated(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size
    ){

        if (page < 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, " @page param must by 0 or higher.");
        } else if (size < 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, " @size param must by 0 or higher.");
        }

        Page<User> resultPage = userService.getAllUsersPaginated(page, size);

        Page<UserBasicInfoDTO> result =
                resultPage.map(user -> UserBasicInfoDTO.getInstanceFromUser(user));

        return ResponseEntity.ok(result);
    }

    @GetMapping(value = "/{userId}")
    @ApiOperation("Get a single User from its Id")
    private ResponseEntity<UserInfoDTO> getUserById(@PathVariable long userId){

        UserInfoDTO result = null;

        try {
            result = UserInfoDTO.getInstanceFromUser(userService.getUserById(userId));
        } catch (NoSuchElementException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with the given @id does not exist.");
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping(value = "/all/export/xml", produces = MediaType.APPLICATION_XML_VALUE)
    @ApiOperation("Get all the Users directly from the FakeAPI instead of BD and create an XML ")
    private ResponseEntity<String> getAllUsersExportXML(){

        String result = null;

        try {
            result = userService.getAllUsersExportXML();
        } catch (JsonProcessingException ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error while creating export string.");
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping(value = "/all/export/xml/file", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ApiOperation("Get all the Users directly from the FakeAPI instead of BD and create an XML ")
    private ResponseEntity<Resource> getAllUsersExportXMLFile(){

        ByteArrayResource result = null;

        try {
            Path filePath = userService.getAllUsersExportXMLFilePath();
            result = new ByteArrayResource(Files.readAllBytes(filePath));
        } catch (IOException ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error while creating export file.");
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-disposition", "attachment; filename=Users-EXP.xml");

        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(result.contentLength())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(result);
    }
}
