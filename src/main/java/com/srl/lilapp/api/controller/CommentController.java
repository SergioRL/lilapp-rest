package com.srl.lilapp.api.controller;

import com.srl.lilapp.api.dto.response.CommentDTO;
import com.srl.lilapp.jpa.entity.Comment;
import com.srl.lilapp.service.comment.CommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(value = "/comment", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class CommentController {

    @Autowired
    private CommentService commentService;

    @GetMapping(value = "/post-id/{postId}/paginated")
    @ApiOperation("Get all the Comments from a Post in a paginated way")
    private ResponseEntity<Page<CommentDTO>> getCommentsByPostIdPaginated(
            @PathVariable Long postId,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size
    ){

        if (page < 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, " @page param must by 0 or higher.");
        } else if (size < 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, " @size param must by 0 or higher.");
        }

        Page<Comment> resultPage = commentService.getCommentsByPostIdPaginated(postId, page, size);

        Page<CommentDTO> result =
                resultPage.map(comment -> CommentDTO.getInstanceFromComment(comment));

        return ResponseEntity.ok(result);
    }

}
