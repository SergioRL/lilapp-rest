package com.srl.lilapp.configuration;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket apiDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.srl.lilapp.api.controller"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getApiInfo())
                ;
    }

    private ApiInfo getApiInfo() {
        return new ApiInfo(
                "Lilapp REST API",
                "Middleware to consume and simulate the modification of FakeAPI [https://jsonplaceholder.typicode.com/]",
                "1.0",
                "http://codmind.com/terms",
                new Contact("Sergio Ríos López", "https://www.linkedin.com/in/SergioRL", "serrilo@hotmail.com"),
                "OPEN LICENSE",
                null,
                Collections.emptyList()
        );
    }
}