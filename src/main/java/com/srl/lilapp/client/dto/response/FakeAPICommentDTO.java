package com.srl.lilapp.client.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FakeAPICommentDTO {

    private Long id;
    private Long postId;
    private String name;
    private String body;
    private String email;

}
