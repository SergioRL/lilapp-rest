package com.srl.lilapp.client.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FakeAPIPostDTO {

    private Long id;
    private Long userId;
    private String title;
    private String body;

}
