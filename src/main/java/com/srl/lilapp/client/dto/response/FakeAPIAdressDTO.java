package com.srl.lilapp.client.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FakeAPIAdressDTO {

    private String street;
    private String suite;
    private String city;
    private String zipcode;

}
