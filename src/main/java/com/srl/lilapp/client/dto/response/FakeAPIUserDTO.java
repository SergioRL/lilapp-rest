package com.srl.lilapp.client.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FakeAPIUserDTO {

    private Long id;
    private String username;
    private String email;
    private String phone;
    private String website;
    private FakeAPIAdressDTO address;

}
