package com.srl.lilapp.client.feign;

import com.srl.lilapp.client.dto.request.CreateCommentDTO;
import com.srl.lilapp.client.dto.request.ModifyPostBodyDTO;
import com.srl.lilapp.client.dto.request.ModifyPostTitleDTO;
import com.srl.lilapp.client.dto.response.FakeAPICommentDTO;
import com.srl.lilapp.client.dto.response.FakeAPIPostDTO;
import com.srl.lilapp.client.dto.response.FakeAPIUserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Implements a Feign Client to consume the FakeAPI REST Service
 */
@FeignClient(name = "${feign.fake-api.name}", url = "${feign.fake-api.base-url}")
public interface FakeAPIFeignClient {

    @GetMapping("/users")
    public List<FakeAPIUserDTO> getAllUsers();

    @GetMapping("/users/{userId}/posts")
    public List<FakeAPIPostDTO> getPostsByUserId(@PathVariable int userId);

    @GetMapping("/posts/{postId}/comments")
    public List<FakeAPICommentDTO> getCommentByPostId(@PathVariable int postId);

    @PostMapping("/comments")
    public FakeAPICommentDTO postComment(@RequestBody CreateCommentDTO createCommentDTO);

    @RequestMapping(value = "/posts/{postId}",
            method = RequestMethod.PATCH,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public FakeAPIPostDTO patchPostTitle(@PathVariable int postId, @RequestBody ModifyPostTitleDTO modifyPostTitleDTO);

    @RequestMapping(value = "/posts/{postId}",
            method = RequestMethod.PATCH,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public FakeAPIPostDTO patchPostBody(@PathVariable int postId, @RequestBody ModifyPostBodyDTO patchCommentBodyDTO);


}
