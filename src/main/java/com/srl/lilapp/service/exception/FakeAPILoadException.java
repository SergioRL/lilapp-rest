package com.srl.lilapp.service.exception;

public class FakeAPILoadException extends RuntimeException {
    public FakeAPILoadException(String s) {
        super(s);
    }
}
