package com.srl.lilapp.service.load;

import org.springframework.stereotype.Service;

@Service
public interface FakeAPILoadService {

    public void loadDataFromFakeAPI();

}
