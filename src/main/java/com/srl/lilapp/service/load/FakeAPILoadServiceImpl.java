package com.srl.lilapp.service.load;

import com.srl.lilapp.client.dto.response.FakeAPICommentDTO;
import com.srl.lilapp.client.dto.response.FakeAPIPostDTO;
import com.srl.lilapp.client.dto.response.FakeAPIUserDTO;
import com.srl.lilapp.client.feign.FakeAPIFeignClient;
import com.srl.lilapp.jpa.entity.Comment;
import com.srl.lilapp.jpa.entity.Post;
import com.srl.lilapp.jpa.entity.User;
import com.srl.lilapp.jpa.repository.CommentRepository;
import com.srl.lilapp.jpa.repository.PostRepository;
import com.srl.lilapp.jpa.repository.UserRepository;
import com.srl.lilapp.service.exception.FakeAPILoadException;
import com.srl.lilapp.service.utils.UserUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Logger;

@Service
public class FakeAPILoadServiceImpl  implements FakeAPILoadService {

    Logger log = Logger.getLogger(FakeAPILoadServiceImpl.class.getName());

    @Autowired
    private FakeAPIFeignClient fakeAPIFeignClient;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Override
    public void loadDataFromFakeAPI() {

        log.info("LOAD START");

        List<FakeAPIUserDTO> allUsers =
                fakeAPIFeignClient.getAllUsers();

        if (allUsers == null || allUsers.isEmpty()) {
            log.warning("No users were found in the FakeAPI.");
        } else {

            allUsers.forEach(
                    user -> {
                        User userEntity = new User();
                        BeanUtils.copyProperties(user,userEntity);
                        userEntity.setCity(user.getAddress() == null ? null : user.getAddress().getCity());

                        // If any exception occurs during save, load continues
                        try{
                            // We load every user with the same pass for test purposes
                            userEntity.setPassword(UserUtils.encryptSHA256("testpass"));

                            userEntity = userRepository.save(userEntity);
                        } catch (NoSuchAlgorithmException ex) {
                            log.warning("Encryption method is not working properly");
                            userEntity = null;
                        }catch (Exception ex) {
                            log.warning(
                                    String.format("Any of the users could not be saved: %d",
                                            user.getId()));
                            userEntity = null;
                        } finally {

                            // If userEntity was persisted neither userEntity nor its ID
                            // should be null
                            if (userEntity != null){

                                //Load posts from saved user
                                loadPostsFromUserIdFakeAPI(userEntity.getId());
                            }
                        }
                    }
            );

        }

        log.info("LOAD END");

    }

    private void loadPostsFromUserIdFakeAPI(Long userId) {

        if (userId == null) {
            throw new FakeAPILoadException("Can't load Posts from a null @userId");
        } else {
            List<FakeAPIPostDTO> postList =
                    fakeAPIFeignClient.getPostsByUserId(userId.intValue());

            postList.forEach(
                    post -> {
                        Post postEntity = new Post();
                        BeanUtils.copyProperties(post, postEntity);

                        // If any exception occurs during save, load continues
                        try{
                            postEntity = postRepository.save(postEntity);
                        } catch (Exception ex) {
                            log.warning(
                                    String.format("Any of the posts could not be saved: %d",
                                            post.getId()));
                            postEntity = null;
                        } finally {

                            // If postEntity was persisted neither postEntity nor its ID
                            // should be null
                            if (postEntity != null){

                                //Load comments from save post
                                loadCommentsFromPostIdFakeAPI(postEntity.getId());
                            }
                        }
                    }
            );
        }

    }

    private void loadCommentsFromPostIdFakeAPI(Long postId) {

        if (postId == null) {
            throw new FakeAPILoadException("Can't load Comments from a null @postId");
        } else {
            List<FakeAPICommentDTO> commentList =
                    fakeAPIFeignClient.getCommentByPostId(postId.intValue());

            commentList.forEach(
                    comment -> {
                        Comment commentEntity = new Comment();
                        BeanUtils.copyProperties(comment, commentEntity);

                        // If any exception occurs during save, load continues
                        try{
                            commentEntity = commentRepository.save(commentEntity);
                        } catch (Exception ex) {
                            log.warning(
                                    String.format("Any of the comments could not be saved: %d",
                                            comment.getId()));
                        }
                    }
            );
        }

    }
}
