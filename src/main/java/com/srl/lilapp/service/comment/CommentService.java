package com.srl.lilapp.service.comment;

import com.srl.lilapp.jpa.entity.Comment;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public interface CommentService {

    public Page<Comment> getCommentsByPostIdPaginated(Long postId, int page, int size);

}
