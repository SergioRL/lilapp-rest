package com.srl.lilapp.service.comment;

import com.srl.lilapp.jpa.entity.Comment;
import com.srl.lilapp.jpa.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImpl implements CommentService{

    @Autowired
    private CommentRepository commentRepository;

    /**
     * Return all the Comments belonging to a Post, paginated in descending order by ID
     *
     * @param postId    ID of the Post which the comments should belong
     * @param page      Number of the page to retrieve
     * @param size      Max number of elements for each page
     * @return          Page number @page with the a number of elements @size
     *                  or less with the given @postId
     */
    @Override
    public Page<Comment> getCommentsByPostIdPaginated(Long postId, int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("id").descending());
        return commentRepository.findByPostId(pageable, postId);
    }
}
