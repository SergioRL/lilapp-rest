package com.srl.lilapp.service.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.srl.lilapp.client.dto.response.FakeAPIUserDTO;
import com.srl.lilapp.client.feign.FakeAPIFeignClient;
import com.srl.lilapp.configuration.properties.ExportFilePathProperties;
import com.srl.lilapp.jpa.entity.User;
import com.srl.lilapp.jpa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Logger;

@Service
public class UserServiceImpl implements UserService {

    Logger log = Logger.getLogger(UserServiceImpl.class.getName());

    @Autowired
    private ExportFilePathProperties exportFileProperties;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FakeAPIFeignClient fakeAPIFeignClient;

    /**
     * Method to get all the users paginated, ordered by username, id
     *
     * @param page      Number of the page to retrieve
     * @param size      Max number of elements for each page
     * @return          Page number @page with the a number of elements @size
     *                  or less
     */
    @Override
    public Page<User> getAllUsersPaginated(int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("username", "id"));
        return userRepository.findAll(pageable);
    }

    /**
     * Returns a User with the given ID if exists, else throws an exception
     *
     * @param userId    ID of the user that will be returned
     * @return          User with the given ID if exists
     * @throws NoSuchElementException
     */
    @Override
    public User getUserById(long userId) throws NoSuchElementException {
        return userRepository.findById(userId).get();
    }

    @Override
    public String getAllUsersExportXML() throws JsonProcessingException {
        List<FakeAPIUserDTO> fakeAPIUserList = fakeAPIFeignClient.getAllUsers();

        XmlMapper xmlMapper = new XmlMapper();
        String xml = xmlMapper.writeValueAsString(fakeAPIUserList);

        return xml;
    }

    @Override
    public Path getAllUsersExportXMLFilePath() throws IOException {

        String fileName = exportFileProperties.getUsers();

        if (!StringUtils.hasText(fileName)) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "No filname was configurated for the Users export.");
        }

        File resultFile = new File(fileName);

        if (!resultFile.exists()) {
            log.warning("User export file will be cretated because it still doesn't exists.");

            List<FakeAPIUserDTO> fakeAPIUserList = fakeAPIFeignClient.getAllUsers();

            resultFile.getParentFile().mkdirs();

            XmlMapper xmlMapper = new XmlMapper();
            xmlMapper.writeValue(resultFile, fakeAPIUserList);
        }

        return resultFile.toPath();

    }
}
