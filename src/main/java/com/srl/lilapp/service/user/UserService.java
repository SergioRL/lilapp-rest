package com.srl.lilapp.service.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.srl.lilapp.jpa.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Path;
import java.util.NoSuchElementException;

@Service
public interface UserService {

    public Page<User> getAllUsersPaginated(int page, int size);

    public User getUserById(long userId) throws NoSuchElementException;

    public String getAllUsersExportXML() throws JsonProcessingException;

    public Path getAllUsersExportXMLFilePath() throws IOException;
}
