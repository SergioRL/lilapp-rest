package com.srl.lilapp.service.post;

import com.srl.lilapp.client.dto.request.ModifyPostBodyDTO;
import com.srl.lilapp.client.dto.request.ModifyPostTitleDTO;
import com.srl.lilapp.client.dto.response.FakeAPIPostDTO;
import com.srl.lilapp.client.feign.FakeAPIFeignClient;
import com.srl.lilapp.jpa.entity.Post;
import com.srl.lilapp.jpa.repository.PostRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private FakeAPIFeignClient fakeAPIFeignClient;

    /**
     * Method to get all the Posts paginated in descending order by ID
     *
     * @param page      Number of the page to retrieve
     * @param size      Max number of elements for each page
     * @return          Page number @page with the a number of elements @size
     *                  or less
     */
    @Override
    public Page<Post> getAllPostsPaginated(int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("id").descending());
        return postRepository.findAll(pageable);
    }

    /**
     * Return all the Posts owned by a User paginated in descending order by ID
     *
     * @param userId    ID of the user who the post belongs to
     * @param page      Number of the page to retrieve
     * @param size      Max number of elements for each page
     * @return          Page number @page with the a number of elements @size
     *                  or less with the given @userId
     */
    @Override
    public Page<Post> getPostsByUserIdPaginated(Long userId, int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("id").descending());
        return postRepository.findByUserId(pageable, userId);
    }

    /**
     * Returns a Post with the given ID if exists, else throws an exception
     *
     * @param postId    The ID of the required Post
     * @return          Post with the given ID if exists
     * @throws NoSuchElementException
     */
    @Override
    public Post getPostById(Long postId)  throws NoSuchElementException {
        return postRepository.findById(postId).get();
    }

    @Override
    public Post patchPostTitle(Long postId, String newTitle) throws NoSuchElementException {

        ModifyPostTitleDTO request = new ModifyPostTitleDTO();
        request.setTitle(newTitle);

        FakeAPIPostDTO responsePost =
                fakeAPIFeignClient.patchPostTitle(postId.intValue(), request);

        Post currentPost = this.getPostById(postId);
        currentPost.setTitle(responsePost.getTitle());

        currentPost = postRepository.save(currentPost);

        return currentPost;
    }

    @Override
    public Post patchPostBody(Long postId, String newBody) throws NoSuchElementException {

        ModifyPostBodyDTO request = new ModifyPostBodyDTO();
        request.setBody(newBody);

        FakeAPIPostDTO responsePost =
                fakeAPIFeignClient.patchPostBody(postId.intValue(), request);

        Post currentPost = this.getPostById(postId);
        currentPost.setBody(responsePost.getBody());

        currentPost = postRepository.save(currentPost);

        return currentPost;
    }


}
