package com.srl.lilapp.service.post;

import com.srl.lilapp.jpa.entity.Post;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
public interface PostService {

    public Page<Post> getAllPostsPaginated(int page, int size);

    public Page<Post> getPostsByUserIdPaginated(Long userId, int page, int size);

    public Post getPostById(Long postId)  throws NoSuchElementException;

    public Post patchPostTitle(Long postId, String newTitle)  throws NoSuchElementException;

    public Post patchPostBody(Long postId, String newBody)  throws NoSuchElementException;

}
