package com.srl.lilapp.service.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class UserUtils {

    public static String encryptSHA256(String s) throws NoSuchAlgorithmException {

        String result = null;

        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(s.getBytes());
        byte[] bytes = md.digest();
        StringBuilder sb = new StringBuilder();
        for(int i=0; i< bytes.length ;i++)
        {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        result = sb.toString();

        return result;
    }

}
