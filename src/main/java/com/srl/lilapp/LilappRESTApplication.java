package com.srl.lilapp;

import com.srl.lilapp.service.load.FakeAPILoadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.EventListener;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication
@EnableFeignClients("com.srl.lilapp.client.feign")
@EnableJpaRepositories("com.srl.lilapp.jpa.repository")
@ComponentScan("com.srl.lilapp")
@EntityScan("com.srl.lilapp.jpa.entity")
public class LilappRESTApplication {

	@Autowired
	private FakeAPILoadService fakeAPILoadService;

	public static void main(String[] args) {
		SpringApplication.run(LilappRESTApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void onStartLoad() {
		fakeAPILoadService.loadDataFromFakeAPI();
	}

}
